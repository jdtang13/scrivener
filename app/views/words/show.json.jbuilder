json.extract! @word, :id, :text, :is_latinate, :syllables_count, :part_of_speech, :is_linking_verb, :canonical, :created_at, :updated_at
