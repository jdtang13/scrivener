json.array!(@words) do |word|
  json.extract! word, :id, :text, :is_latinate, :syllables_count, :part_of_speech, :is_linking_verb, :canonical
  json.url word_url(word, format: :json)
end
