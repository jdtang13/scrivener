json.array!(@results) do |result|
  json.extract! result, :id, :hash, :eloquence_score, :high_register_word_ratio, :mean_word_count, :stddev, :sum_square_sentiment, :adjective_adverb_ratio, :linking_verb_ratio, :redundancy_factor
  json.url result_url(result, format: :json)
end
