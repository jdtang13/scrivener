module WelcomeHelper

def linking_verb_ratio(text, result)
	if (result.blank?)
	puts "result was not cached :("
	return (calculate_vector(text))[0]
	else
	return result[0]
	end
end

def adjective_adverb_ratio(text, result)
	if (result.blank?)
	puts "result was not cached :("
	return (calculate_vector(text))[3]
	else
	return result[3]
	end
end

def high_register_word_ratio(text, result)
	if (result.blank?)
	puts "result was not cached :("
	return (calculate_vector(text))[2]
	else
	puts "result was cached!"
	return result[2]
	end
end

def mean_word_count(text, result)
	if (result.blank?)
	puts "result was not cached :("
	return (calculate_vector(text))[4]
	else
	return result[4]
	end
end

def stddev(text, result)
	if (result.blank?)
	puts "result was not cached :("
	return (calculate_vector(text))[5]
	else
	return result[5]
	end
end

def sentiment(text, result)
	if (result.blank?)
	puts "result was not cached :("
	return calculate_vector(text)[1]
	else
	return result[1]
	end
end

# def high_register_word_ratio(text)
	# if (@result.blank?)
	# puts "@result was not cached :("
	# return (calculate_vector(text))[2]
	# else
	# puts "@result was cached!"
	# return @result[2]
	# end
# end

# def mean_word_count(text)
	# if (@result.blank?)
	# puts "@result was not cached :("
	# return (calculate_vector(text))[4]
	# else
	# return @result[4]
	# end
# end

# def stddev(text)
	# if (@result.blank?)
	# puts "@result was not cached :("
	# return (calculate_vector(text))[5]
	# else
	# return @result[5]
	# end
# end

# def sentiment(text)
	# if (@result.blank?)
	# puts "@result was not cached :("
	# return calculate_vector(text)[1]
	# else
	# return @result[1]
	# end
# end

end
