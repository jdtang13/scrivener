class WelcomeController < ApplicationController
  def index
  end
  
  def analyze
	text = params[:q]
	
	@result = calculate_vector(text)
	# cache the result as @result
	
  end
  
  def calculate_vector(text)
  
    @result = []
	
	text = text.gsub(/[^a-zA-Z0-9.?!:;\s]/, "") #remove all punctuation that isn't sentence-ending
	
    sentences = text.split(%r{(?<=[.?!;:])\s+(?=\p{Lu})})
    totalwordcount = 0.0;
    wordcounts = []
    
    #linking / action verb ratio
    linkingverbs = 0.0
    actionverbs = 0.0
	totalverbs = 0
    
    #adjective/adverb count
    aacount = 0.0
	
    #sentiment analysis
    total_sentiment = 0.0
    
    # high / low register ratio
    lowregwords = 0.0
    highregwords = 0.0
    
    #result vector
    result = []
    
    sentences.each do |sentence|
        wordcount = 0
        words = sentence.gsub(/\s+/m, ' ').strip.split(" ")
        
        # do word specific processing
        words.each do |word|
            
			unless (word == nil or word == ";" or word == ":" or word == ".")
				#puts word
				
				entry = look_up(word)
				#high register / low register
				if is_high_register(entry)
					highregwords += 1
				end
				
				if (entry.part_of_speech == "adjective" or entry.part_of_speech == "adverb")
					aacount += 1
				end
				
				if (entry.is_linking_verb or is_linking_verb(word))
					linkingverbs += 1
				end
				
				wordcount += 1
			end
        end
        
		wordcounts.insert(-1, wordcount)
        totalwordcount += wordcount
        total_sentiment += get_sentiment(sentence)
    end
   
   # calculate mean words and std deviation
   meanwordcount = (1.0 * totalwordcount )/ wordcounts.length
   
   stddev = 0.0
   wordcounts.each do |wordcount|
       temp = (wordcount - meanwordcount) * (wordcount - meanwordcount)
       stddev += temp     
   end
   
   stddev /= wordcounts.length
   stddev = Math.sqrt(stddev)
   
   #calculate mean sentiment
   #get the sentiment of the document as a whole
   mean_sentiment = total_sentiment / (sentences.length * 1.0)
   
   adjective_adverb_ratio = aacount / totalwordcount
   linking_verb_ratio = linkingverbs / totalwordcount

   #consolidate all of the calculated metrics into a vector
   #1) linking verb to action verb ratio
   #2) mean "sentiment" of the text
   #3) ratio of high register words to low register words
   #4) adjective / adverb count
   #5) mean word count of sentences
   #6) standard deviation of sentence length (related to 5)
   #redundancy factor
   
   # TODO: weight all of these
   #result.insert(-1, linkingverbs / actionverbs, mean_sentiment, highregwords / lowregwords, aacount, meanwordcount, stddev
   
   high_register_word_ratio = highregwords / totalwordcount
   
   result.insert(-1, linking_verb_ratio, mean_sentiment, highregwords / totalwordcount, adjective_adverb_ratio, meanwordcount, stddev)
	@result = result
	
	return result
   
   
end
helper_method  :calculate_vector
   
#user, mean, and variance of all of them 
def eloquence_score(result)

	mean = [ 0.05600285714, 0.4419769546, 0.1466140732, 0.4365214286, 25.50477623, 12.68393659]
	variance = [ 0.0003751411604, 0.07120776838, 0.002769618044, 0.001123652582, 55.55155735, 15.47145466]
	weight = [2.0, 1.0, 2.0, 1.0, 2.0, 1.5]
	
    answers = []

	total = 0.0
	
    for i in 0..result.length-1
		gauss = Math.exp(-1*(weight[i]*(result[i]-mean[i])**2)/(2*variance[i]))
        answers.append(gauss)
		total += gauss
    end
	
    return total / 6.0
end
helper_method :eloquence_score

# def sum_square_sentiment(text)
#      sentences = text.split(%r{(?<=[.?!;])\s+(?=\p{Lu})})
#      totalsum += 0
#      sentences.each do |sentence|
#          totalsum += get_sentiment(sentence)**2
# 	end
#      return totalsum
# end

end
