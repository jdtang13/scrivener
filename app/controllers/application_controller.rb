class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
require 'nokogiri'
require 'open-uri'

  	def word_exists(word)
		entry = Word.find_by text:word
		return (entry != nil)
	end
	
	def entry_for(word)
		entry = Word.find_by text:word
	end
	
	def entry_is_incomplete(entry)
	
		#if (entry.is_linking_verb == nil)
		#	return true
		#end
		
		return false
		
	end
	
	def look_up(word)
	
		word = word.downcase
		word = word.gsub("?","")
	
		entry = entry_for(word)
		
		#puts "looking up the word " + word
		
		if (entry != nil and entry.part_of_speech == nil)
			entry.update_attribute("part_of_speech", part_of_speech(word))
		elsif (entry == nil)
		
			#puts "word not found, making it up"
		
			w = Word.new(text: word, is_latinate: is_latinate(word), syllables_count: syllables_count(word),
			is_linking_verb: is_linking_verb(word), part_of_speech: part_of_speech(word))
			w.save
			
			entry = w
		else
			#puts "the word was found"
		end
		
		return entry
	end
	helper_method :look_up
	
	def is_linking_verb(word)
		linking_verbs = [ "be", "am", "is", "are", "was", "were", "shall", "have", "been", "will", "has", "had", "can",
		"may", "might", "should", "could", "become", "would", "appear", "seem"]
		
		return linking_verbs.include?(word)
		
	end
  
	# high register word has more than one syllable and is latinate OR if it has four or more syllables	
	def is_high_register(word)

		s = word.syllables_count
		
		return true if (s >= 4) 
		return true if (s > 1 and word.is_latinate) 
		
		return false
		
	end	
	helper_method :is_high_register

	def wordnik_key
	return "8f7a98ece2c502050b0070ea7420d05f07b7e17ec1aca1b27"
	end
	
	def wordnik_get(word, attribute)
		url = "http://api.wordnik.com/v4/word.xml/" + word + "/" + attribute + "?useCanonical=true&api_key=" + wordnik_key
		xml = Nokogiri::XML(open(url))
		
		return xml
	end

	def idol_key
		return "96e7636b-d66d-4fa5-9b98-c1d6c8f559a3"
	end

	def get_sentiment(sentence)
		url = "https://api.idolondemand.com/1/api/sync/analyzesentiment/v1?apikey=" + idol_key + "&text=" + sentence
		url = URI::encode(url)
		#puts "HELLO WORLD:" + url
		json = JSON.parse(open(url).read())

		aggregate_score = 0.0
		#puts "POS L: " + json["positive"].length.to_s
		#puts "POS N: " + json["negative"].length.to_s
		if !json["aggregate"].length.blank?
			aggregate_score += (json["aggregate"]["score"]).abs
		end

		#puts "AGG SCORE: " + aggregate_score.to_s

		return aggregate_score

	end
	
	def part_of_speech(word)
		url = "http://api.wordnik.com/v4/word.json/" + word + "/definitions?useCanonical=true&api_key=" + wordnik_key
		url = URI::encode(url)
		#puts "HELLO WORLD:" + url
		json = JSON.parse(open(url).read())
		
		# prioritize finding adverbs and adjectives over verbs and nouns
		str = ""
		i = 0
		json.each do |j|
		if (json[i] != nil)
			if (json[i]["partOfSpeech"] == "adjective")
				return "adjective"
			end
			
			if (json[i]["partOfSpeech"] == "adverb")
				return "adverb"
			end
			
			str = json[i]["partOfSpeech"]
			i += 1
		end
		end
		
		return str
		
	end
	  
	def syllables_count(word)
	
		count = 0
		xml = wordnik_get(word, "hyphenation")
		count = xml.to_s.scan(/<syllable/).count - 1
				
		# entry_for(word).update_attribute("syllables_count", count)
		#w = Word.new(:syllables_count => count, text: word)
		return count
		
	end
	helper_method :syllables_count

	def is_latinate(word)
	
		answer = false
		
		xml = wordnik_get(word, "etymologies")
					
		answer = xml.to_s.include?("L.")
		if (answer == false) 
		answer = xml.to_s.include?("F.")
		end
				
		#w = Word.new(is_latinate: answer, text: word)
		return answer
		
	end
	helper_method :is_latinate
  
end
