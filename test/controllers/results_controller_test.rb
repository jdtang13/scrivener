require 'test_helper'

class ResultsControllerTest < ActionController::TestCase
  setup do
    @result = results(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:results)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create result" do
    assert_difference('Result.count') do
      post :create, result: { adjective_adverb_ratio: @result.adjective_adverb_ratio, eloquence_score: @result.eloquence_score, hash: @result.hash, high_register_word_ratio: @result.high_register_word_ratio, linking_verb_ratio: @result.linking_verb_ratio, mean_word_count: @result.mean_word_count, redundancy_factor: @result.redundancy_factor, stddev: @result.stddev, sum_square_sentiment: @result.sum_square_sentiment }
    end

    assert_redirected_to result_path(assigns(:result))
  end

  test "should show result" do
    get :show, id: @result
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @result
    assert_response :success
  end

  test "should update result" do
    patch :update, id: @result, result: { adjective_adverb_ratio: @result.adjective_adverb_ratio, eloquence_score: @result.eloquence_score, hash: @result.hash, high_register_word_ratio: @result.high_register_word_ratio, linking_verb_ratio: @result.linking_verb_ratio, mean_word_count: @result.mean_word_count, redundancy_factor: @result.redundancy_factor, stddev: @result.stddev, sum_square_sentiment: @result.sum_square_sentiment }
    assert_redirected_to result_path(assigns(:result))
  end

  test "should destroy result" do
    assert_difference('Result.count', -1) do
      delete :destroy, id: @result
    end

    assert_redirected_to results_path
  end
end
