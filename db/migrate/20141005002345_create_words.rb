class CreateWords < ActiveRecord::Migration
  def change
    create_table :words do |t|
      t.string :text
      t.boolean :is_latinate
      t.integer :syllables_count
      t.string :part_of_speech
      t.boolean :is_linking_verb #e.g., "is" "was" "were"
      t.string :canonical		 #e.g., "cats" -> "cat", "running" -> "run"

      t.timestamps
    end
  end
end
