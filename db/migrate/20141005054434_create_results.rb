class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.string :unique_string #try to prevent collisions as much as possible
	  t.string :first_word
	  t.integer :word_count
	  
      t.decimal :eloquence_score
      t.decimal :high_register_word_ratio
      t.decimal :mean_word_count
      t.decimal :stddev
      t.decimal :sum_square_sentiment
      t.decimal :adjective_adverb_ratio
      t.decimal :linking_verb_ratio
      t.decimal :redundancy_factor

      t.timestamps
    end
  end
end
